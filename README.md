# Mastodon Ansible playbooks

This is what I use to deploy mastodon.se in both testing and prod environment.

*Disclaimer*; much of this is guesswork based on attemping to deconstruct the otherwise interactive setup process for Mastodon. I'm a Python person with very little Ruby experience so even though these playbooks work, some parts of them might be redundant or unnecessary. Please let me know in the Issues.

# Warning

These playbooks depend on roles I have in my personal, global, roles directory and a private git repo so far.

The roles are quite simple though and I consider them essential.

* sudo\_as - Sets up sudoers config for the users you're logging in with and the user you run mastodon with so that you can use become\_user for all the bundle and yarn commands mastodon needs to run during deploy.
* bootstrap\_common - This one is less necessary and just installs common tools like rsync, nmap, tcpdump and more that I want present on systems for troubleshooting.

The plan is to perhaps get rid of sudo\_as if pipelining and/or acl is enough for become\_user to work. And slim down the installed packages in bootstrap.yml byt getting rid of bootstrap\_common.

# Running playbooks

Read hosts inventory file for variables you should set before running.

Perhaps also check vars/common.yml but you should not have to change this file.

Run playbooks in this order generally.

- bootstrap.yml
- db.yml
- backend.yml
- frontend.yml
- uninstall.yml - Only if you want to start over. Does not remove the DB.

Not included: any media storage solution.

# Upgrade mastodon

    $ ansible-playbook -i hosts.prod upgrade.yml

# Known issues


## Bundler error from upgrade.yml

    ERROR:  Error installing bundler:", "\t\"bundle\" from bundler conflicts with /var/opt/mastodon/.rbenv/versions/2.5.1/bin/bundle

Seems like runninge gem update bundle in upgrade.yml has solved it.

## Error during upgrade.yml

During precompile assets and run yarn tasks it's quite possible for a system to run out of memory. I'd recommend at least 2G for web node and 6G for app node.
