# {{ ansible_managed }}

map $http_upgrade $connection_upgrade {
  default upgrade;
  ''      close;
}

{% if mastodon_enable_tor %}
server {
	include /etc/nginx/mastodon_tor.conf;
}
{% endif %}

server {
  listen 80;
  listen [::]:80;
	server_name {{mastodon_server_name}};
	root {{mastodon_public_web_dir}};

{% if mastodon_use_letsencrypt %}
	# Useful for Let's Encrypt
  location ^~ /.well-known/acme-challenge/ {
    allow all;
    root /var/opt/mastodon/live/public;
    default_type "text/plain";
    try_files $uri =404;
  }
{% endif %}

{% if mastodon_use_ssl %}
	location / { return 301 https://$host$request_uri; }
{% endif %}
}

server {
  set $site_offline "yes";

  # Try to prevent search robots
  add_header X-Robots-Tag "noindex, nofollow, nosnippet, noarchive";

  listen 443 ssl http2;
  listen [::]:443 ssl http2;
	server_name {{mastodon_server_name}};
	root {{mastodon_public_web_dir}};

  ssl_protocols TLSv1.2;
  ssl_ciphers HIGH:!MEDIUM:!LOW:!aNULL:!NULL:!SHA;
  ssl_prefer_server_ciphers on;
  ssl_session_cache shared:SSL:10m;

{% if mastodon_use_letsencrypt %}
	ssl_certificate     /etc/letsencrypt/live/{{mastodon_server_name}}/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/{{mastodon_server_name}}/privkey.pem;
{% elif mastodon_self_sign_ssl %}
	ssl_certificate {{mastodon_home}}/ssl/server.crt;
	ssl_certificate_key {{mastodon_home}}/ssl/server.key;
{% endif %}

	access_log /var/log/nginx/{{mastodon_server_name}}_access.log;
	error_log /var/log/nginx/{{mastodon_server_name}}_error.log;

  include /etc/nginx/mastodon_backend.conf;
}
