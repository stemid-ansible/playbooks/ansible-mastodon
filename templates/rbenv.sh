#!/usr/bin/env bash
# {{ ansible_managed }}

{% for key, val in rbenv_env.items() %}
{{key}}={{val}}
{% endfor %}
