#!/usr/bin/env bash
# {{ ansible_managed }}

ssl_dir={{mastodon_home}}/ssl
server_key="$ssl_dir/server.key"
server_cert="$ssl_dir/server.crt"
server_csr="$ssl_dir/server.csr"

# Delete old files
find $ssl_dir -type f -delete

openssl req -nodes -new -x509 -keyout "$server_key" -out "$server_cert" \
      -subj "{{mastodon_ssl_subject}}" -days 365
