#!/usr/bin/env bash
# {{ ansible_managed }}

rbenv_path="${rbenv_path:-{{rbenv_path}}}"
rbenv_repo="${rbenv_repo:-{{rbenv_env.rbenv_repo}}}"
ruby_build_repo="${ruby_build_repo:-{{rbenv_env.ruby_build_repo}}}"
ruby_version="${ruby_version:-{{rbenv_env.ruby_version}}}"

test -d "$rbenv_path" && rm -rf "$rbenv_path"

git clone $rbenv_repo $rbenv_path
cd $rbenv_path
./src/configure
make -C src

#echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
#echo 'eval "$(rbenv init -)"' >> ~/.bashrc

export PATH="$rbenv_path/bin:$PATH"
eval "$(rbenv init -)"

# Check if rbenv is correctly installed
type rbenv

# Install ruby-build as rbenv plugin
mkdir -p $rbenv_path/plugins
git clone $ruby_build_repo $rbenv_path/plugins/ruby-build

export PATH="$rbenv_path/libexec:$PATH"
echo $SHELL

rbenv install $ruby_version
rbenv global $ruby_version
