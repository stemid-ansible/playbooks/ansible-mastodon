---

- hosts: mastodon-backend
  become: True

  vars_files:
    - vars/common.yml


  roles:
    - role: redis-server
      tags: bootstrap
    - role: rpmfusion_repo
      distro_version: 27
      distro_title: fedora
      tags: bootstrap
    - role: nodejs_repo
      nodejs_version: 8
      tags: bootstrap
    - role: yarn_repo
      tags: bootstrap


  handlers:
    - name: enable sidekiq
      systemd:
        name: mastodon-sidekiq
        enabled: yes
        daemon_reload: yes

    - name: restart sidekiq
      service:
        name: mastodon-sidekiq
        state: restarted
        enabled: yes

    - name: restart mastodon-web
      systemd:
        name: mastodon-web
        state: restarted
        daemon_reload: yes
        enabled: yes

    - name: restart mastodon-streaming
      systemd:
        name: mastodon-streaming
        state: restarted
        enabled: yes
        daemon_reload: yes


  vars:
    mastodon_yum_packages:
      - '@Development tools'
      - ImageMagick
      - protobuf-devel
      - protobuf-compiler
      - ffmpeg
      - readline-devel
      - libicu
      - libicu-devel
      - libffi-devel
      - gdbm-devel
      - gdbm
      - libidn
      - libidn-devel
      - postgresql
      - postgresql-devel
      - nodejs
      - yarn
      - openssl-devel

  tasks:
    - name: create mastodon tmpdir
      file:
        path: "{{mastodon_home}}/tmp"
        state: directory
        owner: "{{mastodon_username}}"
        group: "{{mastodon_username}}"
      tags: bootstrap

    - include: tasks/common-tasks.yml
      tags: bootstrap
    - include: tasks/mastodon.yml
    - include: tasks/rbenv.yml

    - name: install mastodon env file
      template:
        src: templates/mastodon.env
        dest: "{{mastodon_home}}/live/.env.production"
        owner: "{{mastodon_username}}"
        group: "{{mastodon_username}}"

    - name: gem install bundler
      gem:
        name: bundler
        state: present
        executable: "{{ruby_bin_path}}/gem"
      become_user: "{{mastodon_username}}"

    - name: run bundle install for mastodon
      command: "{{ruby_bin_path}}/bundle install -j2 --deployment --without development test"
      args:
        chdir: "{{mastodon_home}}/live"
      become_user: "{{mastodon_username}}"
      register: bundle_install

    - debug: msg="{{bundle_install.stdout}}"

    - name: install sidekiq systemd unit file
      template:
        src: mastodon-sidekiq.service
        dest: /etc/systemd/system/mastodon-sidekiq.service
      notify:
        - enable sidekiq
        - restart sidekiq

    - name: install mastodon-web systemd unit file
      template:
        src: mastodon-web.service
        dest: /etc/systemd/system/mastodon-web.service
      notify: restart mastodon-web

    - name: install mastodon-streaming systemd unit file
      template:
        src: mastodon-streaming.service
        dest: /etc/systemd/system/mastodon-streaming.service
      notify: restart mastodon-streaming

    - name: open necessary ports for mastodon backend
      firewalld:
        port: "{{item}}/tcp"
        permanent: true
        state: enabled
      ignore_errors: yes
      with_items:
        - "{{mastodon_streaming_port}}"
        - "{{mastodon_web_port}}"
        - 6379

    - name: ensure redis is started
      service:
        name: redis
        state: started
        enabled: yes
